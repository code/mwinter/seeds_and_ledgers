\version "2.24.1"

\paper {
  #(set-paper-size "a4" 'portrait)
  top-margin = 1 \cm
  bottom-margin = 1 \cm
  left-margin = 2 \cm
  ragged-bottom = ##t
  
  top-system-spacing =
  #'((basic-distance . 15 )
     (minimum-distance . 15 )
     (padding . 0 )
     (stretchability . 0))
  
  system-system-spacing = 
  #'((basic-distance . 35 )
     (minimum-distance . 35 )
     (padding . 0 )
     (stretchability . 0))
  
  last-bottom-spacing =
  #'((basic-distance . 10 )
     (minimum-distance . 10 )
     (padding . 0 )
     (stretchability . 0))
  
  %systems-per-page = 3
  first-page-number = 1
  print-first-page-number = ##t
  
  print-page-number = ##t
  oddHeaderMarkup = \markup { \fill-line { \line { \unless \on-first-page {\pad-markup #2 { \concat {\italic {"test"}}}}}}}
  evenHeaderMarkup = \markup { \fill-line { \line { \unless \on-first-page {\pad-markup #2 { \concat {\italic {"test"}}}}}}}
  oddFooterMarkup = \markup { \fill-line {
    \concat {
      "-"
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
  evenFooterMarkup = \markup { \fill-line {
    \concat { 
      "-" 
      \fontsize #1.5
      \fromproperty #'page:page-number-string
      "-"}}}
}

%\header {
%  title = \markup { \italic {"seeds and ledgers 3"}}
%  composer = \markup \right-column {"michael winter" "(berlin; 2023-24)"}
%  poet = "for irasema fernandez"
%  tagline = ""
%} 

#(set-global-staff-size 11)

\layout {
  indent = 0.0\cm
  line-width = 17.5\cm 
  ragged-last = ##f
  ragged-right = ##f
  
  \context {
    \Score
    \override BarNumber.stencil = #(make-stencil-circler 0.1 0.25 ly:text-interface::print)
    \override Stem.stemlet-length = #0.75
    %proportionalNotationDuration = #(ly:make-moment 1/16)
    \remove "Separating_line_group_engraver"
    \override RehearsalMark.self-alignment-X = #-1 
    \override RehearsalMark.Y-offset = #10
    \override RehearsalMark.X-offset = #-8
    %\override RehearsalMark.outside-staff-priority = #0
    \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32)
    %\override Stem.stencil = ##f
    %\override BarLine.stencil = ##f
  }
  \context {
    \Staff
    
    \override VerticalAxisGroup.staff-staff-spacing =
    #'((basic-distance . 20 )
       (minimum-distance . 20 )
       (padding . 0 )
       (stretchability . 0))

    \override VerticalAxisGroup.default-staff-staff-spacing =
    #'((basic-distance . 20 )
       (minimum-distance . 20 )
       (padding . 0 )
       (stretchability . 0))
    \override TextScript.staff-padding = #2
    %\override TextScript.self-alignment-X = #0
  }
  \context {
    \StaffGroup
    \name "SemiStaffGroup"
    \consists "Span_bar_engraver"
    \override SpanBar.stencil =
    #(lambda (grob) 
       (if (string=? (ly:grob-property grob 'glyph-name) "|")
           (set! (ly:grob-property grob 'glyph-name) ""))
       (ly:span-bar::print grob))
  }
  \context {
    \Score
    \accepts SemiStaffGroup
  }
}


\score{
  <<
    \new SemiStaffGroup {
      <<
        \new Staff = "I" \with { 
          instrumentName = "I" 
          shortInstrumentName = "I" 
          midiInstrument = #"clarinet" 
        }
        {
          \numericTimeSignature \time 2/2
          \include "includes/part_I.ly"
        }
        \new Staff = "II" \with { 
          instrumentName = "II" 
          shortInstrumentName = "II" 
          midiInstrument = #"clarinet" 
        }
        {
          \include "includes/part_II.ly"
        }
        \new Staff = "III" \with { 
          instrumentName = "III" 
          shortInstrumentName = "III" 
          midiInstrument = #"clarinet" 
          \clef alto
        }
        {
          \include "includes/part_III.ly"
        }
        \new Staff = "IV" \with { 
          instrumentName = "IV" 
          shortInstrumentName = "IV" 
          midiInstrument = #"clarinet" 
          \clef bass
        }
        {
          \include "includes/part_IV.ly"
        }
      >>
    }
  >>
  \layout{}
  %\midi{} %this creates a warning since custom staff is not defined for midi
}
