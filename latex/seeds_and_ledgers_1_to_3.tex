\documentclass[10pt]{article}

\usepackage[a4paper, top=0.5in, bottom=0.7in, left=0.6in, right=0.6in]{geometry}
\usepackage{mathtools}
\usepackage{wasysym}
\usepackage{multicol}
\usepackage{dirtree}
\usepackage{underscore}
\usepackage{pdfpages}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{tocloft}
\usepackage[pangram]{blindtext} 
\usepackage{etaremune}
\usepackage[framed,numbered]{sclang-prettifier}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{lipsum}  
\usepackage{datetime2}
\usepackage{verbatim}
\usepackage{lmodern}
%\usepackage{draftwatermark}
%\DraftwatermarkOptions{scale=0.4}
%\DraftwatermarkOptions{hpos=1.25in, hanchor=c}
%\DraftwatermarkOptions{vpos=1.25in, vanchor=m}

%\usepackage{background}
%\backgroundsetup{
%  position=current page.east,
%  angle=-90,
%  nodeanchor=east,
%  vshift=-5mm, 
%  hshift=0mm, 
%  opacity=0.65,
%  scale=3,
%  contents={TEST VERSION - \today}
%}

\DTMsetdatestyle{default}
\DTMsetup{datesep={.}}

\renewcommand{\cftsecafterpnum}{\hspace*{15em}}
\renewcommand{\cftsubsecafterpnum}{\hspace*{15em}}

\setlength{\parskip}{5pt}
\setlength\parindent{0pt}

\newenvironment{note}{
\vspace{-3mm}
\small
\par
\leftskip=4em\rightskip=5em
\noindent\ignorespaces}{\par\smallskip}


\begin{document}

\setcounter{page}{0}
\pagenumbering{gobble}
\noindent

\large
\textit{\textbf{seeds and ledgers 1--3}}\\
\normalsize
\bigskip
for string quartet\\
dedicated to lucie, cat, and ira, respectively

%\begin{quote}
%\textit{Preferably played in a dark or dim setting (e.g. with the least light %needed by the performers).}
%\end{quote}

\begin{flushright} 
michael winter\\
(berlin and cdmx; 2023--2024) 
\end{flushright} 


\begin{quote}
\renewcommand\contentsname{}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\tableofcontents
\end{quote}

%\newpage
\pagenumbering{arabic}
\phantomsection\addcontentsline{toc}{section}{notes}

\vfill

%\hline
\rule{\textwidth}{0.4pt}

\bigskip
\textbf{Description (which can optionally be used/adapted as a program note)}

The musical processes realized in \textit{seeds and ledgers} explore a reexamination of the traditional concept of `voice leading': how individual melodic lines create and maintain harmonies in aggregate while sometimes modulating. However, this musical question is recontextualized in a phenomenological framework called \textit{just-intonation} in \textit{harmonic space}. In just-intonation, whole number ratios express the frequency relationship between pitches. The resulting musical scales are untempered. They do not favor the accuracy of one interval over another such as with different well- and equal-temperaments, which prioritize and sacrifice the accuracy of different intervals for key cyclicness. 

Traditionally, distance between pitches is typically measured in terms of subjective height expressed in units of semitones or cents (100th of a tempered semitone). This particular concept of a musical space can be referred to as pitch (or melodic) space. However, in harmonic space, distance is measured as a complexity function on the frequency ratio between two tones: the smaller the quantity and size of the prime factors needed to express the numbers in the frequency ratio, the closer they are in harmonic space and relatively easier to tune. For example, the perfect 5th (a frequency ratio of 3/2) is one of the closest intervals in harmonic space but relatively far (7 semitones) in pitch space. On the contrary, smaller melodic and chromatic differences/movements in pitch space are often distant in harmonic space. This gives rise to several vexing musical questions. How is it possible to reconcile these two very different, well-defined measures of distance? How can one tune stepwise movement in pitch space when the relationship between two tones may actually be distant in harmonic space? How can one modulate in harmonic space as the space is by definition acyclic?

In \textit{seeds and ledgers}, these problems are explored using a custom software program that maintains `compact sets' (consonant groups of tones) in harmonic space among any simultaneously sounding tones, but favors smaller steps in pitch space when one voice moves melodically. A compact set is defined as a group of notes such that each note in the group is close in harmonic space to some other note in the group, but not necessarily all of them. When one voice moves melodically, the program will favor notes that move by a smaller step in pitch space within a voice while modulating/transitioning to another compact set among all the voices.  

Any individual part would be near impossible to play by itself. However, because compact sets are always maintained, each successive tone within a part can always be tuned via a relatively simple interval in harmonic space to a tone that is already sounding in one of the other instruments. As a result of this process, some of the passages have an almost baroque, contrapuntal feel; a chromatic drift in harmonic space constantly modulating.

\bigskip
\textbf{Tuning}

%These pieces explore an extended just-intonation with no fixed fundamental. 

Each written note indicates the closest pitch in twelve-tone equal temperament with a cents-deviation (100th of a tempered semitone) provided above. 

As explained in the description above, the note-to-note melodic movement may be very complex. However, each note can always be tuned via a relatively simple interval from a note that is currently sounding or has recently terminated in one of the other parts. For the purposes of the following explanation, the current note will be referred to as the \textit{referencing note} and the pitch against which it can be tuned will be called the \textit{reference pitch}.

\newpage

No indication below a note means that it is the same pitch as the prior note within the respective part.

A Roman numerals below a note indicates the part in which the reference pitch is. The Arabic superscript and the corresponding arrow indicate the exact interval, up or down, of the referencing note from the reference pitch as detailed below. It can be assumed that the reference pitch was initiated before the referencing note and continues to sound. Occasionally, the reference pitch terminates in the beat prior to the initiation of the referencing note. As a courtesy, this is indicated by a `\verb!<!' preceding the Roman numeral below the referencing note.

%A Roman numeral with an Arabic superscript followed by an up or down arrow is most often provided below each note. The Roman numeral indicates the part against which the current note (which will be called the \textit{referencing note} from here on) can be tuned by a relatively simple interval from a recently or currently sounding pitch in the reference part (which will be called the \textit{reference pitch} from here on). The Arabic superscript and the corresponding arrow indicate the exact interval, up or down, of the referencing note from the reference pitch. Generally, the reference pitch is initiated prior and continues to sound when it is referenced. Occasionally, the reference pitch terminates in the beat prior to the initiation of the referencing note. As a courtesy, this is indicated by a `\verb!<!' preceding the Roman numeral below the referencing note.

A Roman numeral with a superscript of 1 below a referencing note means that its pitch is an octave equivalent of the reference pitch.

A Roman Numeral with a superscript of 3, 5, 7, 11, or 13 followed by an up arrow indicates that the pitch of the referencing note is a frequency ratio of a 3/2, 5/4, 7/4, 11/8 or 13/8, respectively, from the reference pitch if the reference pitch were transposed to the nearest octave \textit{below} the pitch of the referencing note.

A Roman Numeral with a superscript of 3, 5, 7, 11, or 13 followed by a down arrow indicates that the pitch of the referencing note is a frequency ratio of a 2/3, 4/5, 4/7, 8/11 or 8/13, respectively, from the reference pitch if the reference pitch were transposed to the nearest octave \textit{above} the pitch of the referencing note. That is, the down arrow is the inversion of an up arrow and it could alternatively be understood that the referencing note is a frequency ratio of 4/3, 8/5, 8/7, 16/11, or 16/13, respectively, from the reference pitch if the reference pitch were transposed to the nearest octave \textit{below} the pitch of the referencing note.

\bigskip
\textbf{Tempo and Duration}

Each of the pieces may be played individually, but it is preferred that they are all played together in sequence. 

Pieces 1 and 2 are written with a 2/2 time signature and should be played at a tempo such that a half note is approximately 60 beats per minute (2 seconds per measure). A number with a fermata above it indicates the exact number of seconds the computer program that generated the piece assigned to the measure. As the fermata implies, these durations can be interpreted more freely by the ensemble.

Piece 3 is more open. Note durations are not provided until measure 105. Within a part, each tone should generally be held until the next tone, but one can occasionally choose to fade out the current tone and rest a moment before initiating the next tone. Among the parts, durations between the initiation of one tone to the next should generally be 1 to 2 seconds.  However, that is more of a lower limit. The performer initiating the next note has discretion as to when that tone begins based on how the current chord contrasts with other chords in the piece. The generating program favors simple chords in some sections and relatively complex chords in others. The performer could, for example, highlight a chord that seem particularly simple or complex by allowing it to sustain for an extended period of time (potentially much longer than 1 to 2 seconds) before initiating the next tone.

Time signatures and note durations are only provided for the end of piece 3 (measures 105 to 114) and an optional coda (starting at measure 115). Like pieces 1 and 2, the ending and the coda of piece 3 should be interpreted with a tempo such that a half note is approximately 60 beats per minute. 

\bigskip
\textbf{Optional Coda}

Given that the coda of piece 3 starting at measure 115 is exceedingly difficult, it is optional. Good luck to any group that attempts it and congratulations in advance to any group who successfully executes it!


\bigskip
\textbf{Generating programs}

These pieces were generated using custom software written in SuperCollider with a front-end user interface developed with Open Stage Control (Javascript). These programs can also be used to audition the pieces and each individual part. The most recent version of the code along with any utilities that are developed are downloadable from a git repository at: \url{https://unboundedpress.org/code/mwinter/seeds_and_ledgers}

The generation of this document (using LaTex) contains a version date at the bottom of this page in order to help track changes and the git repository will also detail commit changes. The piece was last generated using SuperCollider version 3.13 and Lilypond version 2.24.1.

\bigskip
\textbf{Acknowledgement}

A special thanks to Lucie Nezri whose comments, suggestions, and encouragement were integral to the completion of these pieces. 




\vfill
\begin{flushright} 
version generated: \today
\end{flushright} 

\newpage

\cleardoublepage\phantomsection\addcontentsline{toc}{section}{\textit{seeds and ledgers 1}}

\includepdf[pages={-}]{../lilypond/string\string_quartet\string_1/edited/seeds\string_and\string_ledgers\string_1}

\cleardoublepage\phantomsection\addcontentsline{toc}{section}{\textit{seeds and ledgers 2}}

\includepdf[pages={-}]{../lilypond/string\string_quartet\string_2/edited/seeds\string_and\string_ledgers\string_2}

\cleardoublepage\phantomsection\addcontentsline{toc}{section}{\textit{seeds and ledgers 3}}

\includepdf[pages={-}]{../lilypond/string\string_quartet\string_3/main/edited/seeds\string_and\string_ledgers\string_3}

\includepdf[pages={-}]{../lilypond/string\string_quartet\string_3/coda/edited/seeds\string_and\string_ledgers\string_3\string_coda}



\end{document}