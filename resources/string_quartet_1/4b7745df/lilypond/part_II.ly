{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2  a'2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'4 ~ a'16[ r8.] r2  }
 \bar "|"  
 { r1  }
\bar "||"
}