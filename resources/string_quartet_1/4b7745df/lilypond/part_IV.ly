{  
 { r1  }
 \bar "|"  
 { r2.  cis'4^\markup { \pad-markup #0.2 "-19"} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. ~ cis'8.[ r16] }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}