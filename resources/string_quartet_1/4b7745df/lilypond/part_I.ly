{  
 { r1\fermata^6  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1\fermata^2  }
 \bar "|"  
 { r2  r16[ d''8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ d''4 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''4 ~ d''8[ dis''8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }}] ~ dis''2 ~ }
 \bar "|"  
 { dis''1\fermata^2 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''2. ~ dis''8[ r8] }
 \bar "|"  
 { r1\fermata^4  }
\bar "||"
}