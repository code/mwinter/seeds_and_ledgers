{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2  r8[ fis'8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'4 r2.  }
 \bar "|"  
 { r1  }
\bar "||"
}