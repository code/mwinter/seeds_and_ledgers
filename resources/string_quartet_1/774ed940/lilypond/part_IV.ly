{  
 { r8[ dis8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ dis8[ d8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ d2 }
 \bar "|"  
 { dis4^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }} ~ dis8[ f8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ f2 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 }
 \bar "|"  
 { a,1^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { a,2 ~ a,8.[ d16^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ d4 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d8[ r8] r2. }
\bar "|."
}