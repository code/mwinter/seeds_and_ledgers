{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2.  r16[ c'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'16[ gis'8.^\markup { \pad-markup #0.2 "+26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ gis'2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'2 c''2^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { c''8[ gis'8^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ gis'4 ~ gis'8.[ d''16^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ d''4 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''8[ r8] r2. }
\bar "|."
}