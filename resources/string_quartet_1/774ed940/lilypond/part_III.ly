{  
 { g1^\markup { \pad-markup #0.2 "-2"} ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g2. ~ g16[ a8.^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { a2 ~ a16[ gis8.^\markup { \pad-markup #0.2 "+26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ gis4 ~ }
 \bar "|"  
 { gis4 ~ gis16[ d8.^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ d2 ~ }
 \bar "|"  
 { d16[ ais8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }}] ~ ais8[ ais8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ ais2 }
 \bar "|"  
 { b2^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }} c'2^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { c'8[ fis8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ fis2. ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis8[ r8] r2. }
\bar "||"
}