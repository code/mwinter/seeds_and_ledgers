{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r8[ a'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↓" }}] ~ a'2. ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'4 ~ a'16[ r8.] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}