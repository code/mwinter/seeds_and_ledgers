{  
 { r1\fermata^6  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1\fermata^2  }
 \bar "|"  
 { r4 c''2.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''2 cis''2^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { cis''1\fermata^2 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''4 ~ cis''16[ r8.] r2  }
 \bar "|"  
 { r1\fermata^4  }
\bar "||"
}