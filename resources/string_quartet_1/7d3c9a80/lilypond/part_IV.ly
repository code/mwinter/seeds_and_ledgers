{  
 { r1  }
 \bar "|"  
 { r8[ cis'8^\markup { \pad-markup #0.2 "-19"}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 ~ cis'8.[ r16] r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}