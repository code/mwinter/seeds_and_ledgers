{  
 { fis'1^\markup { \pad-markup #0.2 "-21"} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'4 ~ fis'8.[ r16] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}