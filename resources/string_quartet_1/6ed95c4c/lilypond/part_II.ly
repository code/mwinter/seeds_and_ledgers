{  
 { r1  }
 \bar "|"  
 { r2.  gis'4^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { gis'2. ~ gis'8.[ ais'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'4 ~ ais'16[ gis'8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ gis'2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'8[ g'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ g'2. ~ }
 \bar "|"  
 { g'2. ~ g'8.[ r16] }
 \bar "|"  
 { r1 }
  \bar "|"  
 { r1 }
\bar "||"
}