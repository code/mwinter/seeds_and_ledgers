{  
 { r1  }
 \bar "|"    
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8.[ dis16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ dis2 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis8[ d8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ d2. ~ }
 \bar "|"  
 { d8.[ c16^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ c2. ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
\bar "||"
}