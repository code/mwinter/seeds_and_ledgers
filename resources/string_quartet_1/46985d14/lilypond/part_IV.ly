{  
 { r2.  r8[ gis8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { gis2 g2^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g4 ~ g8[ fis8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ fis2 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis8.[ fis16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ fis8.[ gis16^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ gis2 ~ }
 \bar "|"  
 { gis8.[ g16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ g2 ~ g8.[ g16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { g2 r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r16[ fis8.^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ fis2. }
 \bar "|"  
 { g4^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }} ~ g8[ g8^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ g2 }
 \bar "|"  
 { fis2^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} fis2^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { fis8.[ g16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ g2. ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g2 ~ g16[ r8.] r4 }
 \bar "|"  
 { r1  }
\bar "||"
}