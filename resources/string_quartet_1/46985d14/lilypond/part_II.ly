{  
 { ais1^\markup { \pad-markup #0.2 "+49"} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. ~ ais8.[ a16^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { a4 a2.^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }} ~ }
 \bar "|"  
 { a16[ b8.^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }}] ~ b4 ~ b8[ a8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ a4 ~ }
 \bar "|"  
 { a8[ gis8^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }}] ~ gis8[ a8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }}] ~ a2 ~ }
 \bar "|"  
 { a8.[ ais16^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ ais2 ~ ais8.[ a16^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 ais2^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais8.[ r16] r2.  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}