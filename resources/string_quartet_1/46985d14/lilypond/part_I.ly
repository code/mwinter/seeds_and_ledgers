{  
 { r2.  r8[ d'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 ~ d'8[ dis'8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ dis'4 ~ dis'16[ d'8.^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2 cis'2^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 r2  }
 \bar "|"  
 { r8.[ cis'16^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1\fermata^2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1\fermata^8 ~ }
\bar "||"
}