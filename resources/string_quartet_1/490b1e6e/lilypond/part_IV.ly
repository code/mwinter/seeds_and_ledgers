{  
 { r2  r8.[ c'16^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ c'4 ~ }
 \bar "|"  
 { c'8[ ais8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ ais4 ~ ais16[ a8.^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ a8.[ ais16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { ais2 ~ ais8.[ gis16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ gis4 ~ }
 \bar "|"  
 { gis4 g2.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { g16[ fis8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ fis2. ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis4 ~ fis16[ r8.] r2 }
\bar "||"
}