{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2.  r8[ ais8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
\bar "||"
}