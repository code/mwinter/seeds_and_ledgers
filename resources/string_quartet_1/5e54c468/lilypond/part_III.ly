{  
 { fis'1^\markup { \pad-markup #0.2 "-21"} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. ~ e'8[ dis'8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { dis'2 dis'2^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ f'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ f'4 ~ f'8[ fis'8^\markup { \pad-markup #0.2 "-21"}] ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2. r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}