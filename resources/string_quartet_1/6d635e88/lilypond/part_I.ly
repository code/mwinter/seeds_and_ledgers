{  
 { r1  }
 \bar "|"  
 { r1\fermata^2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1\fermata^2  }
 \bar "|"  
 { r2  r8.[ ais'16^\markup { \pad-markup #0.2 "-35"}] ~ ais'4 ~ }
 \bar "|"  
 { ais'1\fermata^2 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1\fermata^2 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1\fermata^2 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1\fermata^2 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1\fermata^2 ~ }
 \bar "|"  
 { ais'8.[ c''16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ c''2. ~ }
 \bar "|"  
 { c''8[ ais'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }}] ~ ais'2. ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'8.[ r16] r2.  }
 \bar "|"  
 { r1\fermata^4  }
\bar "||"
}