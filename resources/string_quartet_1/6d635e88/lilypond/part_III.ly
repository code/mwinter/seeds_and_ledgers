{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8[ fis'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }}] ~ fis'4 ~ fis'8.[ gis'16^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'4 ~ gis'16[ fis'8.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }}] ~ fis'2 }
 \bar "|"  
 { f'1^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { f'4 ~ f'8[ e'8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'4 ~ e'16[ d'8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ d'2 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2. dis'4^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'16[ r8.] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2.  r8[ dis'8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'16[ d'8.^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ d'4 ~ }
 \bar "|"  
 { d'8.[ c'16^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ c'2. ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8.[ r16] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}