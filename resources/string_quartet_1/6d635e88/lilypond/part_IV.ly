{  
 { c1^\markup { \pad-markup #0.2 "-4"} ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c2. r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2.  r8.[ d16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d4 ~ d8[ dis8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ dis2 ~ }
 \bar "|"  
 { dis4 ~ dis8[ f8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ f2 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f4 ~ f8[ e8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ e2 ~ }
 \bar "|"  
 { e4 ~ e16[ dis8.^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ dis2 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 }
 \bar "|"  
 { r1  }
\bar "||"
}