{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8[ c8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ c2 ~ }
 \bar "|"  
 { c8[ ais,8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ ais,8.[ a,16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ a,2 ~ }
 \bar "|"  
 { a,2 r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
\bar "||"
}