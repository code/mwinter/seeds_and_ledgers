{  
 { ais'1^\markup { \pad-markup #0.2 "+41"} ~ }
 \bar "|"  
 { ais'2 ~ ais'8[ r8] r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1\fermata^4  }
\bar "||"
}