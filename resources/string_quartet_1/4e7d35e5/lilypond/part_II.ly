{  
 { r2.  r8[ g'8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 ~ g'8[ gis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ gis'4 fis'4^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis'4 ~ fis'8.[ r16] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}