{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8[ c8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ c2 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c4 ~ c16[ r8.] r2  }
 \bar "|"  
 { r1 }
\bar "||"
}