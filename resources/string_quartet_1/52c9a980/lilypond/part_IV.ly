{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2  c2^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { c4 ais,4^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ ais,16[ a,8.^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ a,4 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r2  }
 \bar "|"  
 { r1 }
\bar "||"
}