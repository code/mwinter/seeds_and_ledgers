{  
 { r1  }
 \bar "|"  
 { r2.  c'4^\markup { \pad-markup #0.2 "+49"} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'16[ r8.] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}