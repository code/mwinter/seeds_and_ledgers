{  
 { r1  }
 \bar "|"  
 { g'1^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g'2. f'4^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'16[ gis'8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ gis'4 ~ gis'8.[ a'16^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'2. ~ a'8.[ ais'16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'2 ~ ais'8[ r8] r4 }
 \bar "|"  
 { r1 }
\bar "||"
}