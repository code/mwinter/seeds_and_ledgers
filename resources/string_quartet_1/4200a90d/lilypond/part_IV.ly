{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2.  c4^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { c16[ ais,8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ ais,4 ~ ais,8.[ a,16^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ a,4 ~ }
 \bar "|"  
 { a,4 ~ a,16[ g,8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ g,2 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~}
\bar "||"
}