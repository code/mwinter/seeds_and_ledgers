{  
 { ais'1^\markup { \pad-markup #0.2 "+41"} ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'2 ~ ais'8.[ a'16^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ a'4 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2 fis'2^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2. ~ fis'16[ r8.]}
\bar "||"
}