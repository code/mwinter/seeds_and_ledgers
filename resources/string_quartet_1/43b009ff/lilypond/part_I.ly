{  
 { r1\fermata^4  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 b'2.^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { b'2. ~ b'8[ b'8^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'2. ~ b'8[ r8] }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1\fermata^6  }
\bar "||"
}