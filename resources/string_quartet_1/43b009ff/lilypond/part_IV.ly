{  
 { r1  }
 \bar "|"  
 { r2.  r8[ cis'8^\markup { \pad-markup #0.2 "-19"}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 ~ cis'16[ r8.] r2  }
 \bar "|"  
 { r1  }
\bar "||"
}