{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8[ c8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ c2 ~ }
 \bar "|"  
 { c8[ a,8^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ a,2. ~ }
 \bar "|"  
 { a,2. ~ a,16[ r8.] }
 \bar "|"  
 { r1 }
\bar "||"
}