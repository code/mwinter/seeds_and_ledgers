{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r8[ cis'8^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~}
\bar "||"
}