{  
 { r2.  r8[ f'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { f'8.[ fis'16^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ fis'4 ~ fis'8[ e'8^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↓" }}] ~ e'4 ~ }
 \bar "|"  
 { e'4 ~ e'16[ fis'8.^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ fis'16[ gis'8.^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ gis'4 ~ }
 \bar "|"  
 { gis'8.[ a'16^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ a'2. ~ }
 \bar "|"  
 { a'2. ~ a'8.[ fis'16^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { fis'2. ~ fis'16[ g'8.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { g'2 ~ g'8.[ a'16^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2 a'2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2. ~ a'16[ ais'8.^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { ais'4 ~ ais'8.[ cis''16^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ cis''2 ~ }
 \bar "|"  
 { cis''16[ d''8.^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ d''2 ~ d''16[ dis''8.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { dis''1}
\bar "||"
}