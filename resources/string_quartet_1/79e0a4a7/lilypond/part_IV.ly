{  
 { r1  }
 \bar "|"  
 { r4 r8[ cis'8^\markup { \pad-markup #0.2 "-19"}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { r1  }
\bar "||"
}