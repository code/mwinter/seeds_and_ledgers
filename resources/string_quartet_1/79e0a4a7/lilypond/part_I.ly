{  
 { r1\fermata^4  }
 \bar "|"   
 { r1  }
 \bar "|"  
 { r1\fermata^2  }
 \bar "|"  
 { r2  a'2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'8[ a'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↓" }}] ~ a'2. ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2 ~ a'8[ r8] r4 }
 \bar "|"  
 { r1\fermata^6  }
\bar "||"
}