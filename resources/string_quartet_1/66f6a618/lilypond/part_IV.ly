{  
 { r1  }
 \bar "|"  
 { r4 r16[ gis8.^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ gis2 ~ }
 \bar "|"  
 { gis16[ fis8.^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ fis4 e4^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ e8[ dis8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { dis4 d2.^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d4 b,4^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} ~ b,8[ dis8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }}] ~ dis4 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis2 ~ dis16[ dis8.^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ dis4 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 }
 \bar "|"  
 { cis2^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} dis2^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} }
 \bar "|"  
 { d2.^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ d16[ cis8.^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }}] ~ }
 \bar "|"  
 { cis4 b,2^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} ~ b,8[ c8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"   
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"   
 { c8.[ r16] r2.  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}