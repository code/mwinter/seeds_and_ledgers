{  
 { r4 r8[ b8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ b2 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2. ~ b8.[ a16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 ~ a8.[ gis16^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ gis4 ~ }
 \bar "|"  
 { gis2 ~ gis16[ gis8.^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}] ~ gis4 ~ }
 \bar "|"  
 { gis2 fis2^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"    
 { fis4 f2.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f8.[ gis16^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ gis2. }
 \bar "|"  
 { fis2.^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ fis16[ e8.^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { e4 ~ e16[ e8.^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ e2 ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"    
 { e1 ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e4 ~ e16[ d8.^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }}] ~ d2 ~ }
 \bar "|"  
 { d4 cis2^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ cis8[ d8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { d2. ~ d8[ dis8^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~}
\bar "||"
}