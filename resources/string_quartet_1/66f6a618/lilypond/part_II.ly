{  
 { r2  r8[ b8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ b4 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"   
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"   
 { b2 ~ b8.[ r16] r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}