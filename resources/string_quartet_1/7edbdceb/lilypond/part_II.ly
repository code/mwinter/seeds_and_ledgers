{  
 { r2.  r8[ f'8^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 ~ f'8[ e'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ e'4 f'4^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'2. ~ f'8.[ r16] }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}