{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8[ c8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ c2 ~ }
 \bar "|"  
 { c8[ ais,8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ ais,2. ~ }
 \bar "|"  
 { ais,2. ~ ais,16[ r8.] }
 \bar "|"  
 { r1 }
\bar "||"
}