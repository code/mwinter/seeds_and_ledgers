{  
 { r1  }
 \bar "|"  
 { r2  r8[ c'8^\markup { \pad-markup #0.2 "+49"}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 r2  }
 \bar "|"  
 { r1 }
\bar "||"
}