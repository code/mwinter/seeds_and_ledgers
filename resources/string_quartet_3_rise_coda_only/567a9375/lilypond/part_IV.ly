{  
 { ais,2.^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "< II"\normal-size-super " 3↓" }} c8^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}[ cis8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ } 
 { cis8[ d8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] }
\bar "||"
}