{  
 { d''8^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "< I"\normal-size-super " 7↑" }}[ f''8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] g''8^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }}[ ais'8^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] gis'4^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} f''2^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} }
 
\bar "||"
}