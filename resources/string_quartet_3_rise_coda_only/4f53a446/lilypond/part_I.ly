{  
 { ais1^\markup { \pad-markup #0.2 "-45"} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 ~ ais16[ ais8.^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ ais4 ~ ais8[ a8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { a4 gis2.^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 \laissezVibrer}
 \break
\bar "||"
}