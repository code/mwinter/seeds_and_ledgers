{  
 { f'1^\markup { \pad-markup #0.2 "-43"} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 ~ f'16[ fis'8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ fis'2 ~ }
 \bar "|"  
 { fis'4 gis'2.^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 \laissezVibrer}
\bar "||"
}