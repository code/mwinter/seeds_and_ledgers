{  
 { d'1^\markup { \pad-markup #0.2 "+26"} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 \laissezVibrer}
\bar "||"
}