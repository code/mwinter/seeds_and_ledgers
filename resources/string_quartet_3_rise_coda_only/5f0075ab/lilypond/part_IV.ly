{  
 { f,4^\markup { \pad-markup #0.2 "-38"} g,2^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ g,8[ f8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] }
 { fis8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}[ f8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] }
\bar "||"
}