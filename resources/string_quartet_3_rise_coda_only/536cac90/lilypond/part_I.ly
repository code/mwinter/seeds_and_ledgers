{  
  \time 2/2
 { f'2^\markup { \pad-markup #0.2 "-38"} g'2^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { g'8[ ais'8^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ ais'2. ~ }
 \bar "|"
\time 7/8
 { ais'2 ~ ais'4.}
\bar "|"
}