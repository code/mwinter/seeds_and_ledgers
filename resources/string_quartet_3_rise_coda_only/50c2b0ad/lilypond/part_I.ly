{  
 { cis''2^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "< II"\normal-size-super " 1↑" }} d'2^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 \time 7/8
 { dis'2 ~ dis'4. ~}
  \time 2/2
\bar "||"
}