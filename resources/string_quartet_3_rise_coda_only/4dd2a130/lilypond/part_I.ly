{  
  \time 3/2
 { f'1.^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "< IV"\normal-size-super " 1↑" }} ~ }
\bar "||"
}