{  
 { cis'1^\markup { \pad-markup #0.2 "+46"} }
 \bar "|"  
 { a'1^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { a'1}
\bar "||"
}