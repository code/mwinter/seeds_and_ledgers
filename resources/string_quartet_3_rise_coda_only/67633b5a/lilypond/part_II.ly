{  
 { dis''4^\markup { \pad-markup #0.2 "-42"} ~ dis''8[ e''8^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ e''2 }
 \bar "|"  
 { d''1^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { d''1}
\bar "||"
}