{  
 { ais'1^\markup { \pad-markup #0.2 "-40"} }
 \bar "|"  
 { d''1^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { d''1}
\bar "||"
}