{  
 { r2  e'2^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { e'1\fermata^2 ~ }
 \bar "|"  
 { e'2. ~ e'8.[ e'16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { e'1\fermata^4 ~ }
 \bar "|"  
 { e'4 ~ e'16[ f'8.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1\fermata^4}
\bar "||"
}