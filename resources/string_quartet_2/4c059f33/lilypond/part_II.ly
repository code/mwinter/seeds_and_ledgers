{  
 { r2  cis'2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. ~ cis'8.[ cis'16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"   
 { cis'4 ~ cis'16[ d'8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ d'2 ~ }
 \bar "|"  
 { d'1}
\bar "||"
}