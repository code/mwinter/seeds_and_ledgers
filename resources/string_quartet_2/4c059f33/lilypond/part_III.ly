{  
 { r2  a2^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2. ~ a8.[ cis16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"   
 { cis4 ~ cis16[ cis8.^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ cis2 ~ }
 \bar "|"  
 { cis1}
\bar "||"
}