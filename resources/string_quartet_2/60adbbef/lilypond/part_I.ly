{  
 { r1\fermata^2  }
 \bar "|"  
 { r2.  r8[ a'8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { a'1\fermata^4 ~ }
 \bar "|"  
 { a'4 fis'2.^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }} ~ }
 \bar "|"  
 { fis'1\fermata^4 ~ }
 \bar "|"    
 { fis'2. ~ fis'16[ g'8.^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { g'1\fermata^4 ~ }
 \bar "|"   
 { g'16[ a'8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ a'2. ~ }
 \bar "|"  
 { a'1\fermata^4 ~ }
 \bar "|"  
 { a'2 r2  }
 \bar "|"  
 { r1 \fermata^8 }
\bar "||"
}