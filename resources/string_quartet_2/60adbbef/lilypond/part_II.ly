{  
 { r1  }
 \bar "|"  
 { r2.  r8[ cis'8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"   
 { cis'4 d'2.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2. ~ d'16[ dis'8.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'16[ e'8.^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }}] ~ e'2. ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r2  }
 \bar "|"  
 { r1  }
\bar "||"
}