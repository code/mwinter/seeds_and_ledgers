{  
 { c'1^\markup { \pad-markup #0.2 "+0"} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'16[ e'8.^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'2 ~ e'8.[ g'16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ g'4 ~ }
 \bar "|"  
 { g'4 ~ g'8[ ais'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ ais'2 ~ }
 \bar "|"  
 { ais'4 ~ ais'8.[ gis'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }}] ~ gis'2 }
 \bar "|"  
 { gis'2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ gis'8[ g'8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ g'4 ~ }
 \bar "|"  
 { g'8[ f'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ f'2. ~ }
 \bar "|"  
 { f'4 ~ f'8[ e'8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ e'2 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'16[ fis'8.^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ fis'2. ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'16[ a'8.^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ a'2 ~ a'16[ ais'8.^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'2 ~ ais'16[ b'8.^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ b'4 ~ }
 \bar "|"  
 { b'2 ~ b'8[ a'8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'2 ~ a'8.[ fis'16^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'16[ f'8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ f'2. ~ }
 \bar "|"  
 { f'4 ~ f'8.[ e'16^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 ~ e'16[ r8.] r2  }
 \bar "|"  
 { r1 }
\bar "||"
}