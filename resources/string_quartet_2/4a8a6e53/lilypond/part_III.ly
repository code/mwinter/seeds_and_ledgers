{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r16[ ais8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ ais2. ~ }
 \bar "|"  
 { ais2. gis4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { gis16[ fis8.^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }}] ~ fis2. ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2 ~ fis8.[ r16] r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}