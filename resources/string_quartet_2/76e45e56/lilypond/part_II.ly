{  
 { r2  dis''2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { dis''1  }
 \bar "|"  
 { cis''1^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''2. d''4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''2. b'4^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'2 ~ b'16[ a'8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"    
 { a'2. ~ a'16[ r8.] }
\bar "||"
}