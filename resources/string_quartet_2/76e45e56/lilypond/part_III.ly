{  
 { r2  cis'2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. f'4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. fis'4^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"   
 { fis'2 ~ fis'16[ g'8.^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ g'4 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'2. ~ g'16[ r8.] }
\bar "||"
}