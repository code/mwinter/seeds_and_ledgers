{  
 { r2  e'2^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { e'1\fermata^6 }
 \bar "|"   
 { e'1^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { e'1\fermata^2 ~ }
 \bar "|"  
 { e'2. d'4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { d'1\fermata^2 ~ }
 \bar "|"  
 { d'2. cis'4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { cis'1\fermata^4 ~ }
 \bar "|"  
 { cis'2 ~ cis'16[ cis'8.^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ cis'4 ~ }
 \bar "|"  
 { cis'1\fermata^4 ~ }
 \bar "|"  
 { cis'2. ~ cis'16[ r8.] }
\bar "||"
}