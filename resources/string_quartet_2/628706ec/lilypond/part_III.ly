{  
 { ais'1^\markup { \pad-markup #0.2 "+18"} ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 }
 \bar "|"  
 { f''2^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ f''8.[ e''16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ e''4 ~ }
 \bar "|"  
 { e''16[ dis''8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ dis''2. ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''2. d''4^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }} ~ }
 \bar "|"  
 { d''8[ d''8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ d''4 ~ d''8[ c''8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ c''4 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''4 ~ c''16[ b'8.^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ b'16[ b'8.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }}] ~ b'4 ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'4 ~ b'8[ b'8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ b'2 ~ }
 \bar "|"  
 { b'4 ~ b'16[ c''8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} cis''16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ cis''2 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1}
\bar "||"
}