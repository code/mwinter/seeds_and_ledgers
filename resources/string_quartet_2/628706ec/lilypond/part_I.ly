{  
 { r1  }
 \bar "|"  
 { r4 r8[ ais''8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ ais''2 ~ }
 \bar "|"  
 { ais''16[ g''8.^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ g''4 ~ g''8.[ r16] r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2.  r8.[ g''16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { g''2 ~ g''8.[ f''16^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ f''4 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''2. ~ f''16[ e''8.^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''2 ais''8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}[ a''16^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ a''4 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''4 gis''8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ dis'''8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ dis'''2 ~ }
 \bar "|"  
 { dis'''8.[ d'''16^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ d'''4 ~ d'''8.[ cis'''16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ cis'''4 ~ }
 \bar "|"  
 { cis'''1 ~ }
 \bar "|"  
 { cis'''1 ~ }
 \bar "|"  
 { cis'''1 ~ }
 \bar "|"  
 { cis'''8.[ f''16^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↓" }}] ~ f''2. ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''8.[ f''16^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] fis''2.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { fis''2 ~ fis''16[ g''8.^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ g''8[ r8]}
\bar "||"
}