{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r16[ ais'8.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ ais'2. ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'2 ~ ais'16[ r8.] r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r2  r16[ a'8.^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'8.[ a'16^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ a'2. ~ }
 \bar "|"  
 { a'2 gis'2^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'4 ~ gis'8.[ gis'16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ gis'4 ~ gis'8.[ a'16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { a'2 a'2^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }} ~ }
 \bar "|"  
 { a'8.[ b'16^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ b'2 ~ b'8.[ gis'16^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { gis'4 ~ gis'8[ a'8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ a'2}
\bar "||"
}