{  
 { r1\fermata^2 }
 \bar "|"  
 { r16[ c''8.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ c''2. ~ }
 \bar "|"  
 { c''1\fermata^2 ~ }
 \bar "|"  
 { c''4 ~ c''8.[ e'16^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ e'2 ~ }
 \bar "|"  
 { e'2. ~ e'8[ e'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { e'1\fermata^2 ~ }
 \bar "|"  
 { e'2 ~ e'16[ dis'8.^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1\fermata^2 ~}
\bar "||"
}