{  
 { r1  }
 \bar "|"  
 { r2.  r8[ e8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"   
 { e4 f2.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"   
 { f2. ~ f16[ cis'8.^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'16[ cis'8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"   
 { cis'2 d'2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"   
 { d'8[ f8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ f2. ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f8.[ r16] r2.  }
 \bar "|"  
 { r1  }
\bar "||"
}