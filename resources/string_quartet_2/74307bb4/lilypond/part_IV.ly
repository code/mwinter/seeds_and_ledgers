{  
 { r1  }
 \bar "|"  
 { r2.  r8.[ c'16^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. r4 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 r8.[ b16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ b2 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b8.[ ais16^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ ais2. ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. ~ ais8[ b8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2. ~ b8[ c'8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 cis'2.^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { cis'2. ~ cis'8[ cis'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. d'4^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2. ~ d'16[ dis'8.^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'8[ e8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }}] ~ e2. ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e4 ~ e8[ e8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ e2 ~ }
 \bar "|"  
 { e2 ~ e8.[ dis16^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ dis4 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis4 ~ dis8.[ e16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ e2 ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e2 ~ e8[ e8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}] ~ e4 ~ }
 \bar "|"  
 { e1}
\bar "||"
}