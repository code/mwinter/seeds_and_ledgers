{  
 { dis'1^\markup { \pad-markup #0.2 "+16"} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1\fermata^4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1\fermata^2 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1\fermata^2 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. e'4^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }} ~ }
 \bar "|"  
 { e'1\fermata^2 ~ }
 \bar "|"  
 { e'2. ~ e'8.[ f'16^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { f'1\fermata^2 ~ }
 \bar "|"  
 { f'8.[ fis'16^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ fis'2 ~ fis'8[ fis'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { fis'1\fermata^2 ~ }
 \bar "|"  
 { fis'2. g'4^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g'2 ~ g'16[ gis'8.^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ gis'4 ~ }
 \bar "|"  
 { gis'8[ a'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ a'2. ~ }
 \bar "|"  
 { a'1\fermata^2 ~ }
 \bar "|"  
 { a'2 ~ a'8[ a'8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'4 ~ a'8.[ b'16^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ b'2 ~ }
 \bar "|"  
 { b'1\fermata^2 ~ }
 \bar "|"  
 { b'2. ~ b'8.[ e'16^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { e'1\fermata^2 }
 \bar "|"  
 { fis'1^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { fis'1\fermata^4 ~ }
 \bar "|"   
 { fis'8.[ f'16^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ f'2. ~ }
 \bar "|"  
 { f'1\fermata^2 ~ }
 \bar "|"  
 { f'8.[ fis'16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ fis'2. ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'8.[ g'16^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }}] ~ g'2. ~ }
 \bar "|"  
 { g'2 ~ g'16[ gis'8.^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ gis'4 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^4 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1\fermata^2 ~ }
 \bar "|"  
 { gis'4 ~ gis'8[ g'8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ g'2 ~ }
 \bar "|"  
 { g'1\fermata^2 ~ }
 \bar "|"  
 { g'2 ~ g'8[ fis'8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'1\fermata^2 ~ }
 \bar "|"  
 { fis'2. ~ fis'8[ cis''8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { cis''1\fermata^4 ~ }
 \bar "|"  
 { cis''4 ~ cis''16[ cis'8.^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1\fermata^2 ~ }
 \bar "|"  
 { cis'4 ~ cis'8.[ b16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }}] ~ b2 ~ }
 \bar "|"  
 { b2 ais2^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { ais2 b2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { b16[ dis'8.^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'4 ~ dis'8.[ e'16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'1\fermata^2 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1\fermata^4}
\bar "||"
}