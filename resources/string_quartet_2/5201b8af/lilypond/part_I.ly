{  
 { r2  c''2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { c''1\fermata^2 ~ }
 \bar "|"  
 { c''2. ~ c''8.[ e'16^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { e'1\fermata^4 ~ }
 \bar "|"  
 { e'4 ~ e'16[ e'8.^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'1\fermata^4}
 \bar "|"  
 { r1\fermata^6 }
\bar "||"
}