{  
 { r2  e'2^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. ~ e'8.[ e'16^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 ~ e'16[ f'8.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1}
 \bar "|"  
 { r1 }
\bar "||"
}