{  
 { r2  fis'2^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2. ~ fis'8.[ g'16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 ~ g'16[ gis'8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ gis'2 ~ }
 \bar "|"  
 { gis'1}
 \bar "|"  
 { r1 }
\bar "||"
}