{  
 { c'1^\markup { \pad-markup #0.2 "+0"} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ c'8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ c'2 ~ }
 \bar "|"  
 { c'2 b2^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~}
  \bar "|" 
  {b1}
   \bar "|" 
  {r1}
  \bar "||"
}