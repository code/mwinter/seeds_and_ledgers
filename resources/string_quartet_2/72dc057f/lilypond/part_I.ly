{  
 { r1\fermata^4  }
 \bar "|"   
 { r2  e'2^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { e'1\fermata^4 ~ }
 \bar "|"  
 { e'2. ~ e'8.[ e'16^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { e'1\fermata^2 ~ }
 \bar "|"  
 { e'2. ~ e'8[ d'8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { d'1\fermata^4 ~ }
 \bar "|"    
 { d'2. ~ d'8.[ r16] }
 \bar "|"  
 { r1\fermata^6  }
\bar "||"
}