{  
 { r1  }
 \bar "|"   
 { r2  cis'2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"   
 { cis'2. ~ cis'8.[ cis'16^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. ~ cis'8[ f'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. ~ f'8.[ r16] }
 \bar "|"  
 { r1  }
\bar "||"
}