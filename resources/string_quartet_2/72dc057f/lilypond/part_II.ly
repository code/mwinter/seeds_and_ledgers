{  
 { r1  }
 \bar "|"   
 { r2  dis''2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"   
 { dis''2. ~ dis''8.[ cis''16^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''2. ~ cis''8[ d''8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"   
 { d''2. ~ d''8.[ r16] }
 \bar "|"  
 { r1  }
\bar "||"
}