{  
 { r1  }
 \bar "|"  
 { r8[ g8^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ g2. ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g2 cis2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis4 ~ cis8[ cis8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ cis2 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis2. ~ cis8.[ d16^\markup { \pad-markup #0.2 "+14"}] ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d2 dis2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { dis2 e2^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e1 ~}
  \bar "|"  
 { e1}
  \bar "|"  
 { r1}
\bar "||"
}