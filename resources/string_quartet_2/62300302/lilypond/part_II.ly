{  
 { r1  }
 \bar "|"  
 { r2.  r8[ d'8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'8[ dis'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ e'8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ e'4 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 b2.^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b16[ cis'8.^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ cis'4 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { d'1^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { d'1 ~} 
  \bar "|"  
 { d'1 } 
  \bar "|"  
 { r1 } 
\bar "||"
}