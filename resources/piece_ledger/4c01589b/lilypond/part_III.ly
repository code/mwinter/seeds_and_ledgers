{  
 { r2.  r8[ c'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { c'2 ais2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { ais4 ~ ais8[ a8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ a2 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a8.[ a16^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ a8.[ b16^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }}] ~ b2 ~ }
 \bar "|"  
 { b8.[ ais16^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ ais2 ~ ais8.[ b16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { b2 r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r16[ ais8.^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ ais2. }
 \bar "|"  
 { ais4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }} ~ ais8[ ais8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ ais2 }
 \bar "|"  
 { a2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} a2^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { a8.[ b16^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ b2. ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b16[ r8.] r2. }
\bar "||"
}