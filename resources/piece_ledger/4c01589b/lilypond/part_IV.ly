{  
 { r2.  r8[ ais8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}] ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 ~ ais8[ c'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ c'4 ~ c'16[ e'8.^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { e'2 ~ e'8[ g'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ g'4 ~ }
 \bar "|"  
 { g'2 a'2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'8.[ r16] r2.  }
 \bar "|"  
 { r1 }
\bar "||"
}