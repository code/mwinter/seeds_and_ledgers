{  
 { r2.  r8[ fis'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'4 ~ fis'8[ g'8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ g'4 ~ g'16[ fis'8.^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }}] ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 e'2^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r2  }
 \bar "|"  
 { r8.[ e'16^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ e'2. ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'16[ r8.] r2. }
\bar "||"
}