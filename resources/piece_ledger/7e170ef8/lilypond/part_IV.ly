{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r8[ d'8^\markup { \pad-markup #0.2 "+0"}] ~ d'2. ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 ~ d'8.[ r16] r2 }
\bar "||"
}