{  
 { r2.  r8[ fis'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { fis'8.[ g'16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ g'4 ~ g'8[ fis'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'4 ~ fis'16[ gis'8.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ gis'16[ a'8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ a'4 ~ }
 \bar "|"  
 { a'8.[ ais'16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ ais'2. ~ }
 \bar "|"  
 { ais'2. ~ ais'8.[ gis'16^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { gis'2. ~ gis'16[ gis'8.^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { gis'2 ~ gis'8.[ ais'16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ ais'4 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'2 ais'2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'2. ~ ais'16[ c''8.^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { c''4 ~ c''8.[ d''16^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ d''2 ~ }
 \bar "|"  
 { d''16[ dis''8.^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ dis''2 ~ dis''16[ e''8.^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { e''4 ~ e''8.[ r16] r2 }
\bar "||"
}