{  
 { r2.  r8[ c'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'16[ a8.^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ a16[ ais8.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}] ~ ais4 ~ }
 \bar "|"  
 { ais8.[ ais16^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ ais2. ~ }
 \bar "|"  
 { ais2. ~ ais8.[ c'16^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 ~ c'8.[ ais16^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↑" }}] ~ ais4 ~ }
 \bar "|"  
 { ais2 ~ ais16[ ais8.^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. ~ ais8[ a8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { a4 ~ a8[ c'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ c'4 ~ c'8.[ e'16^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { e'2. dis'4^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ fis'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'8.[ g'16^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ g'2. ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 ~ g'8.[ r16] r2 }
\bar "||"
}