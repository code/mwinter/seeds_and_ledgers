{  
 { s4 s4 s4 s4 }
   
 { d'4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} s4 dis'4^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} s4 }
   
 { s4 ais'4^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { b'4^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} s4 s4 s4}

}