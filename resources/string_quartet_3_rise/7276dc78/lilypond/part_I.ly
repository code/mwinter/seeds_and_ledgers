{  
 { s4 s4 gis4^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} s4 }
   
 { d'4^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} s4 s4 gis'4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} }
   
 { s4 s4 s4 s4 }
   
 { cis''4^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} s4 s4 a4^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 s4 dis'4^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} }
   
 { b'4^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 s4 c''4^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} }
   
 { s4 s4 s4 s4 }
   
 { c'4^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 e'4^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} s4 a'4^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} }
   
 { s4 cis''4^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} s4}

}