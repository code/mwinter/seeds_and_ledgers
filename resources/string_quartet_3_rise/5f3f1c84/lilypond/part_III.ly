{  
 {s4 s4 s4 s4 }
   
 { s4 s4 s4 f'4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} }
   
 { s4 s4 s4 s4 }
   
 { s4 c''4^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }} s4 s4 }
   
 { cis''4^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↓" }} s4 fis4^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 }
   
 { cis'4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} gis'4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} s4 s4 }
   
 { f'4^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} s4 s4}

}