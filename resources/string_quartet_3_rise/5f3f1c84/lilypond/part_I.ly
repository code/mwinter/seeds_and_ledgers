{  
 { f'4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 s4 s4 }
   
 { s4 s4 a'4^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} s4 }
   
 { s4 a'4^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 c''4^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} s4 }
   
 { s4 g4^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} a4^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} \bar "||"}

}