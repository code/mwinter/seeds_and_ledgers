{  
 { d'8^\markup { \pad-markup #0.2 "+0"}[ g8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] a8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ ais8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] ~ ais8[ c'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] d'4^\markup { \pad-markup #0.2 "+0"} ~ }
 \bar "|"  
 { d'8[ ais8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ ais8[ c'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ c'4 d'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ e'8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { e'4 f'4^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} a2^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} }
 \bar "|"  
 { b2^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} c'2^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} }
 \bar "|"  
 { cis'4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} ~ cis'8[ fis'8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] b8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}[ a'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] e'4^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { e'4 ~ e'8[ f'8^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] g'8^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}[ gis'8^\markup { \pad-markup #0.2 "+15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ gis'8[ ais8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] }
 \bar "|"  
 { c'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}[ ais'8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ ais'8[ dis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] f'2^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} }
 \bar "|"  
 { fis'2^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} b'4^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ais'8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}[ e'8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] }
 \bar "|"  
 { fis'4^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ fis'8[ g'8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ g'8[ c'8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] g''4^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { g''2 gis''2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} }
 \bar "|"  
 { a''4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ a''8[ e''8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ e''4 fis''4^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} }
 \bar "|"  
 { cis''2.^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} dis''8^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }}[ cis''8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] }
 \bar "|"  
 { d''2^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} ~ d''8[ dis''8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ dis''4 ~ }
 \bar "|"  
 { dis''4 d''2.^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { d''4 a'8^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}[ b'8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ b'2 ~ }
 \bar "|"  
 { b'4 e'2.^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}}
\bar "||"
}