{  
 { dis'2^\markup { \pad-markup #0.2 "+38"} dis'2^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { dis'8[ f'8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ f'2. ~ }
 \bar "|"  
 \time 9/8
 { f'2. ~ f'4.}
\bar "||"
}