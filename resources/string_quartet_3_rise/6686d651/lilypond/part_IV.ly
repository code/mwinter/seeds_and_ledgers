{  
 { fis'4^\markup { \pad-markup #0.2 "+43"} ~ fis'8[ gis'8^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ gis'2 ~ }
 \bar "|"  
 { gis'2 ~ gis'8[ ais'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ ais'4 ~ }
 \bar "|"  
 { ais'2 ~ ais'8[ b'8^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] dis,4^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { dis,2 dis,8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}[ e,8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] fis,8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ gis,8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] }
 \bar "|"  
 { b,8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}[ cis8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] dis8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}[ dis'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] b2^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} }
 \bar "|"  
 { cis'8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}[ b8^\markup { \pad-markup #0.2 "+15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↓" }}] ~ b8[ b8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ b2 }
 \bar "|"  
 { b4^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} c'8^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}[ cis'8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ cis'8[ c'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ c'8[ ais8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] }
 \bar "|"  
 { a8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ d,8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] d2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} ~ d8[ e8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e4 f4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} g8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}[ d8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ d4 }
 \bar "|"  
 { a,1^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a,8[ gis,8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] e2.^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { e4 f8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}[ g8^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] f'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ g'8^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }}] ~ g'4 ~ }
 \bar "|"  
 { g'2 c'2^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { c'4 ~ c'8[ dis'8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ dis'8[ e'8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ e'4 }
 \bar "|"  
 { fis'8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}[ dis'8^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ dis'2 ~ dis'8[ e'8^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] }
 \bar "|"  
 { cis'8^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}[ c'8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] c'8^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}[ cis'8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ cis'2}
\bar "||"
}