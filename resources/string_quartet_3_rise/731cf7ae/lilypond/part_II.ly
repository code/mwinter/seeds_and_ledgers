{  
 { cis'2^\markup { \pad-markup #0.2 "-29"} gis8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}[ ais8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] c'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}[ cis'8^\markup { \pad-markup #0.2 "-29"}] }
 \bar "|"  
 { dis'8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}[ e'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ e'2 dis'8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}[ c''8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] }
 \bar "|"  
 { cis''8^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}[ dis''8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] gis'4^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} c'4^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ c'8[ cis'8^\markup { \pad-markup #0.2 "-29"}] }
 \bar "|"  
 { f'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}[ fis'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ fis'8[ e'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ e'2 }
 \bar "|"  
 { dis'8^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}[ f'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ f'8[ fis'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ fis'4 ~ fis'8[ f'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 ~ f'8[ gis8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ gis2 ~ }
 \bar "|"  
 { gis2 g2^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { g2. a8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}[ d'8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] }
 \bar "|"  
 { e'8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}[ g'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ g'4 g2^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { g2 a8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}[ ais8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] b4^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b2. a8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}[ b8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] }
 \bar "|"  
 { c'4^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ c'8[ d'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] d'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}[ cis'8^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] b8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}[ ais8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] }
 \bar "|"  
 { a1^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a4 ~ a8[ b8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ais8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}[ c'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ c'8[ d'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { d'4 cis'4^\markup { \pad-markup #0.2 "-29"} ~ cis'8[ b8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ b4 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b4 ais8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}[ f''8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}] ~ f''2 ~ }
 \bar "|"  
 { f''8[ fis''8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }}] cis''4^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} dis''8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }}[ e''8^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ e''4}
\bar "||"
}