{  
 { s4 s4 s4 s4 }
   
 { s4 cis''4^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} s4 s4 }
   
 { s4 s4 cis'4^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} s4 }
   
 { s4 s4 s4 dis'4^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} }
   
 { s4 s4 e'4^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 }
   
 { s4 s4 g'4^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} s4 }
   
 { s4 cis''4^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} s4 s4 }
   
 { s4 gis'4^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} s4 s4 }
   
 { s4 s4 ais'4^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} s4 }
   
 { s4 s4 b'4^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} s4 }
   
 { d''4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} s4 s4 cis''4^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} }
   
 { s4 s4}

}