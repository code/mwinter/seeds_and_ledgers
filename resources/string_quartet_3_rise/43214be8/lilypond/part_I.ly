{  
 { ais'1^\markup { \pad-markup #0.2 "-11"} }
 \bar "|"  
 { gis1^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} }
 \bar "|"  
 { d'1^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { d'2 gis'2^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 }
 \bar "|"  
 { cis''1^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { cis''2 a2^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 dis'2^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} }
 \bar "|"  
 { b'1^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { b'2 c''2^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 e'2^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { e'2 a'2^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a'2 cis''2^\markup { \pad-markup #0.2 "+50"}}
\bar "||"
}