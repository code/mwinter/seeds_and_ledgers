{  
 { a'1^\markup { \pad-markup #0.2 "+36"} ~ }
 \bar "|"  
 { a'2 cis''2^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b2 e'2^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 }
 \bar "|"  
 { dis1^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { dis2 f'2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} }
 \bar "|"  
 { c''1^\markup { \pad-markup #0.2 "-37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''2 e2^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e2 a2^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1}
\bar "||"
}