{  
 { e'1^\markup { \pad-markup #0.2 "+38"} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 }
 \bar "|"  
 { a'1^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a'1 }
 \bar "|"  
 { cis''1^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { cis''2 e'2^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↓" }} }
 \bar "|"  
 { b'1^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} }
 \bar "|"  
 { g'1^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { g'1 }
 \bar "|"  
 { g'1^\markup { \pad-markup #0.2 "-35"} ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 }
 \bar "|"  
 { cis''1^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} }
 \bar "|"  
 { a1^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 d'2^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2 g'2^\markup { \pad-markup #0.2 "-35"} ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1}
\bar "||"
}