{  
 { d'2^\markup { \pad-markup #0.2 "+0"} a'2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { a'2 ais'2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 ~ }
 \bar "|"  
 { ais'1 }
 \bar "|"  
 { b'1^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { b'2 cis''2^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { cis''1 }
 \bar "|"  
 { d''1^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { d''2 e'2^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 fis'2^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 cis''2^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis''2 cis''2^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { cis''2 b'2^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'2 c''2^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { c''2 cis''2^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''2 d'2^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { d'2 e'2^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 f'2^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { g'2^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} gis'2^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { gis'1 }
 \bar "|"  
 { a'2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} b'2^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1 }
 \bar "|"  
 { b'1^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { b'2 cis''2^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 }
 \bar "|"  
 { d''2^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }} b'2^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1}
\bar "||"
}