{  
 { d1^\markup { \pad-markup #0.2 "+0"} ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 }
 \bar "|"  
 { dis1^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} }
 \bar "|"  
 { f1^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 }
 \bar "|"  
 { fis1^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2 gis2^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis2 a2^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 }
 \bar "|"  
 { ais1^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { ais2 e,2^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { e,2 fis,2^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { fis,2 g,2^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { a,2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} b,2^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 }
 \bar "|"  
 { gis1^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis1 }
 \bar "|"  
 { a1^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a1 }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} }
 \bar "|"  
 { d'2^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} dis'2^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { dis'1 }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 fis'2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }}}
\bar "||"
}