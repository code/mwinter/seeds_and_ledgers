{  
 { s4 s4 \clef treble d'4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} s4 }
   
 { a'4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} s4 s4 s4 }
   
 { s4 \clef bass cis4^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} s4 gis4^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} }
   
 { d'4^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4}

}