{  
 { g'4^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 s4 d''4^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }} }
   
 { s4 a4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 s4 }
   
 { s4 s4 cis'4^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} s4 }
   
 { s4 c''4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} s4 s4 }
   
 { s4 c''4^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↑" }} s4 s4 }
   
 { s4}

}