{  
 {s4 s4 s4 s4 }
   
 { s4 s4 cis''4^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 cis''4^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 13↑" }} d''4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} }
   
 { s4 s4 fis'4^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }} s4 }
   
 { g'4^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}}

}