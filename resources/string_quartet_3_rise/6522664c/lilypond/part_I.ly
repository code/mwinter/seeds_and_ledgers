{  
 { a'4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} s4 s4 s4 }
   
 { gis4^\markup { \pad-markup #0.2 "+26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} s4 d'4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} s4 }
   
 { fis'4^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} s4 s4 s4 }
   
 { s4 gis'4^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} s4 s4 }
   
 { a'4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} s4 s4}

}