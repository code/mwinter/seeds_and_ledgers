{  
 { s4 s4 s4 s4 }
   
 { a'4^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} s4 s4 s4 }
   
 { s4 a'4^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} s4 s4 }
   
 { f'4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} s4 f'4^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} c''4^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} }
   
 { s4 s4 s4 cis''4^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}}

}