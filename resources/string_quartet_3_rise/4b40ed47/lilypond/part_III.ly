{  
 { s4 s4 s4 ais'4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 b'4^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↓" }} s4 s4 }
   
 { d''4^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} s4 e'4^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} s4}

}