{  
 { s4 s4 s4 d4^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 s4 g4^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} }
   
 { s4 s4 \clef treble ais'4^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} \clef bass e4^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} }
   
 { s4 s4 a4^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} b,4^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} }
   
 { s4 g4^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} s4 \clef treble b'4^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} }
   
 { s4 \clef bass fis4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} s4 s4 }
   
 { s4 b4^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }} s4 s4 }
   
 { fis'4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} s4 s4 s4 }
   
 { fis4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} s4 s4 s4 }
   
 { s4 s4 s4 fis,4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} }
   
 { s4 s4 s4 c4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} }
   
 { s4 f4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}}

}