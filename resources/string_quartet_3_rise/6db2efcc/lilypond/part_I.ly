{  
 { c'4^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} s4 s4 a'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↓" }} }
   
 { ais'4^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 b'4^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} s4 }
   
 { s4 s4 s4 s4}

}