{  
 { s4 s4 gis'4^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} s4 }
   
 { s4 s4 s4 s4 }
   
 { s4 s4 b'4^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} s4 }
   
 { s4 s4 c''4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} s4 }
   
 { s4 s4 s4 cis''4^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 13↑" }}}

}