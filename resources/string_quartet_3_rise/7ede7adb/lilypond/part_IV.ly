{  
 { ais'4^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} s4 d''4^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1↑" }} s4 }
   
 { s4 \clef bass a,4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} s4 s4 }
   
 { e4^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }} s4 s4 s4 }
   
 { s4 s4 s4 s4 }
   
 { g4^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}}

}