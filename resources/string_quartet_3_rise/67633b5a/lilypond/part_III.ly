{  
 { dis'8^\markup { \pad-markup #0.2 "-42"}[ cis'8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ cis'2 ~ cis'8[ a'8^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { a'1}
\bar "||"
}