{  
 { dis''4^\markup { \pad-markup #0.2 "-42"} e''2^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }} ~ e''8[ d''8^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { d''1}
\bar "||"
}