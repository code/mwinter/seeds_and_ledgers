{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 e'2.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↓" }} }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { dis'4 r2.  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { f'1^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { f'2. ~ f'8[ fis'8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { fis'8.[ gis'16^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ gis'2. ~ }
 \bar "|"  
 { gis'4 ~ gis'8[ r8] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}