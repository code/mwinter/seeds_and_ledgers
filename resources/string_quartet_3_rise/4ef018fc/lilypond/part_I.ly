{  
 { r1  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r4 c'4^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↓" }} ~ c'8[ b8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 13↓" }}] ~ b4 }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. ~ cis'8[ dis'8^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}