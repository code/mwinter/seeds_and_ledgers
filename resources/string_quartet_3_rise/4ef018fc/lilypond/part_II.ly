{  
 { d'1^\markup { \pad-markup #0.2 "+0"} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2 ~ d'8[ gis8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 1↑" }}] ~ gis4 }
 \bar "|"  
 { ais1^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { ais2 ~ ais16[ b8.^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ b4 ~ }
 \bar "|"  
 { b4 ~ b8.[ a16^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ a2 ~ }
 \bar "|"  
 { a2 ~ a16[ gis8.^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "IV"\normal-size-super " 3↑" }}] ~ gis4 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis2. ~ gis8.[ r16] }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}