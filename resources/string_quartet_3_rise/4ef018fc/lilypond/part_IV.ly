{  
 { r1  }
 \bar "|"  
 { r2.  r8[ gis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'4 a'4^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 13↑" }} ~ a'16[ b'8.^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1↑" }}] ~ b'4 ~ }
 \bar "|"  
 { b'4 ~ b'8.[ cis''16^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ cis''2 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''2. ~ cis''8[ dis''8^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1↑" }}] ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''4 ~ dis''8[ r8] r2  }
 \bar "|"  
 { r1  }
 \bar "|"  
 { r1 }
\bar "||"
}